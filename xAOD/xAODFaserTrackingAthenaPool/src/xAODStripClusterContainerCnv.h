#ifndef XAODFASERTRACKINGATHENAPOOL_XAODSTRIPCLUSTERCONTAINERCNV_H
#define XAODFASERTRACKINGATHENAPOOL_XAODSTRIPCLUSTERCONTAINERCNV_H

// Gaudi/Athena include(s):
#include "AthenaPoolCnvSvc/T_AthenaPoolCustomCnv.h"

// EDM include(s):
#include "xAODFaserTracking/StripClusterContainer.h"

/// Type definition for the converter's base
typedef T_AthenaPoolCustomCnv< xAOD::StripClusterContainer,
                               xAOD::StripClusterContainer >
   xAODStripClusterContainerCnvBase;

/**
 *  @short POOL converter for the xAOD::StripClusterContainer class
 *
 *         Simple converter class making the xAOD::StripClusterContainer
 *         class known to POOL.

 *
 */
class xAODStripClusterContainerCnv : public xAODStripClusterContainerCnvBase {

   // Declare the factory as our friend:
   friend class CnvFactory< xAODStripClusterContainerCnv >;

public:
   /// Converter constructor
   xAODStripClusterContainerCnv( ISvcLocator* svcLoc );
   
   /// Re-implemented function in order to get access to the SG key
   virtual StatusCode createObj( IOpaqueAddress* pAddr, DataObject*& pObj );
   
   /// Function preparing the container to be written out
   virtual xAOD::StripClusterContainer* createPersistent( xAOD::StripClusterContainer* trans );
   
   /// Function reading in the persistent object
   virtual xAOD::StripClusterContainer* createTransient();

private:
   /// Function preparing a vertex object for persistence
   void toPersistent( xAOD::StripCluster* np ) const;

   /// StoreGate key of the container just being created
   std::string m_key;

}; // class xAODStripClusterContainerCnv

#endif 