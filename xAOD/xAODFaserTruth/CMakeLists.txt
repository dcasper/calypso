# $Id: CMakeLists.txt 761796 2016-07-14 08:06:02Z krasznaa $
################################################################################
# Package: xAODFaserTruth
################################################################################

# Declare the package name:
atlas_subdir( xAODFaserTruth )

# Component(s) in the package:
atlas_add_library( xAODFaserTruth
   xAODFaserTruth/*.h xAODFaserTruth/versions/*.h Root/*.h Root/*.cxx
   PUBLIC_HEADERS xAODFaserTruth
   LINK_LIBRARIES AthContainers AthLinks xAODBase xAODFaserBase xAODCore
   PRIVATE_LINK_LIBRARIES TruthUtils )

atlas_add_dictionary( xAODFaserTruthDict
   xAODFaserTruth/xAODFaserTruthDict.h
   xAODFaserTruth/selection.xml
   LINK_LIBRARIES xAODFaserTruth
   EXTRA_FILES Root/dict/*.cxx )

atlas_generate_cliddb( xAODFaserTruth )

# Test(s) in the package:
atlas_add_test( ut_xaodfasertruth_particle_test
   SOURCES test/ut_xaodfasertruth_particle_test.cxx
   LINK_LIBRARIES xAODFaserTruth )
