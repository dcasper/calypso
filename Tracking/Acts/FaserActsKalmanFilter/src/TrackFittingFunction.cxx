#include "FaserActsKalmanFilterAlg.h"
#include "FaserActsGeometry/FASERMagneticFieldWrapper.h"

#include "Acts/Propagator/EigenStepper.hpp"
#include "Acts/Propagator/Navigator.hpp"
#include "Acts/Propagator/Propagator.hpp"
#include "Acts/TrackFitting/GainMatrixSmoother.hpp"
#include "Acts/TrackFitting/GainMatrixUpdater.hpp"


namespace {

using Updater = Acts::GainMatrixUpdater;
using Smoother = Acts::GainMatrixSmoother;
using Stepper = Acts::EigenStepper<>;
using Propagator = Acts::Propagator<Stepper, Acts::Navigator>;
using Fitter = Acts::KalmanFitter<Propagator, Updater, Smoother>;

struct TrackFitterFunctionImpl
    : public FaserActsKalmanFilterAlg::TrackFitterFunction {
  Fitter trackFitter;

  TrackFitterFunctionImpl(Fitter &&f) : trackFitter(std::move(f)) {}

  FaserActsKalmanFilterAlg::TrackFitterResult operator()(
      const std::vector<IndexSourceLink> &sourceLinks,
      const FaserActsKalmanFilterAlg::TrackParameters &initialParameters,
      const FaserActsKalmanFilterAlg::TrackFitterOptions &options)
  const override {
    return trackFitter.fit(sourceLinks, initialParameters, options);
  };
};

}  // namespace


std::shared_ptr<FaserActsKalmanFilterAlg::TrackFitterFunction>
FaserActsKalmanFilterAlg::makeTrackFitterFunction(
    std::shared_ptr<const Acts::TrackingGeometry> trackingGeometry) {
  auto magneticField = std::make_shared<FASERMagneticFieldWrapper>();
  auto stepper = Stepper(std::move(magneticField));
  Acts::Navigator::Config cfg{trackingGeometry};
  cfg.resolvePassive = false;
  cfg.resolveMaterial = true;
  cfg.resolveSensitive = true;
  Acts::Navigator navigator(cfg);
  Propagator propagator(std::move(stepper), std::move(navigator));
  Fitter trackFitter(std::move(propagator));
  return std::make_shared<TrackFitterFunctionImpl>(std::move(trackFitter));
}

/*

namespace ActsExtrapolationDetail {
using VariantPropagatorBase = boost::variant<
  Acts::Propagator<Acts::EigenStepper<>, Acts::Navigator>,
  Acts::Propagator<Acts::EigenStepper<>, Acts::Navigator>>;

class VariantPropagator : public VariantPropagatorBase {
public:
  using VariantPropagatorBase::VariantPropagatorBase;
};
}  // namespace ActsExtrapolationDetail


namespace {
template <typename Fitter>
struct FitterFunctionImpl {
  Fitter fitter;
  FitterFunctionImpl(Fitter&& f) : fitter(std::move(f)) {}
  FaserActsKalmanFilterAlg::TrackFitterResult operator()(
    const std::vector<IndexSourceLink>& sourceLinks,
    const Acts::CurvilinearTrackParameters& initialParameters,
    const Acts::KalmanFitterOptions<MeasurementCalibrator, Acts::VoidOutlierFinder, Acts::VoidReverseFilteringLogic>& options) const {
      return fitter.fit(sourceLinks, initialParameters, options);
    };
};
}  // namespace

std::shared_ptr<FaserActsKalmanFilterAlg::TrackFitterFunction>
FaserActsKalmanFilterAlg::makeTrackFitterFunction(std::shared_ptr<const Acts::TrackingGeometry> trackingGeometry) {

  const std::string fieldMode = "FASER";
  const std::vector<double> constantFieldVector = {0., 0., 0.55};

  Acts::Navigator::Config cfg{trackingGeometry};
  cfg.resolvePassive   = false;
  cfg.resolveMaterial  = true;
  cfg.resolveSensitive = true;
  Acts::Navigator navigator( cfg );

  std::unique_ptr<ActsExtrapolationDetail::VariantPropagator> varProp;

  if (fieldMode == "FASER") {
    auto bField = std::make_shared<FASERMagneticFieldWrapper>();
    auto stepper = Acts::EigenStepper<>(std::move(bField));
    auto propagator = Acts::Propagator<decltype(stepper),
        Acts::Navigator>(std::move(stepper), std::move(navigator));
    varProp = std::make_unique<ActsExtrapolationDetail::VariantPropagator>(propagator);
  } else if (fieldMode == "Constant") {
    Acts::Vector3 constantFieldVector = Acts::Vector3(
      constantFieldVector[0], constantFieldVector[1], constantFieldVector[2]);
    auto bField = std::make_shared<Acts::ConstantBField>(constantFieldVector);
    auto stepper = Acts::EigenStepper<>(std::move(bField));
    auto propagator = Acts::Propagator<decltype(stepper),
        Acts::Navigator>(std::move(stepper), std::move(navigator));
    varProp = std::make_unique<ActsExtrapolationDetail::VariantPropagator>(propagator);
  }

  return boost::apply_visitor([&](const auto& propagator) -> TrackFitterFunction {
    using Updater  = Acts::GainMatrixUpdater;
    using Smoother = Acts::GainMatrixSmoother;
    using Fitter = Acts::KalmanFitter<typename std::decay_t<decltype(propagator)>, Updater, Smoother>;
    Fitter fitter(std::move(propagator));
    return FitterFunctionImpl<Fitter>(std::move(fitter));
  }, *varProp);
}
 */
