#ifndef MCEVENTS_MCEVENTSALG_H
#define MCEVENTS_MCEVENTSALG_H

#include "AthenaBaseComps/AthReentrantAlgorithm.h"
#include "TrackerSimEvent/FaserSiHitCollection.h"
#include "GeneratorObjects/McEventCollection.h"

class FaserSCT_ID;

class MCEventsAlg : public AthReentrantAlgorithm {
public:
  MCEventsAlg(const std::string& name, ISvcLocator* pSvcLocator);
  virtual ~MCEventsAlg() = default;

  virtual StatusCode initialize() override;
  virtual StatusCode execute(const EventContext& ctx) const override;
  virtual StatusCode finalize() override;

private:
  const FaserSCT_ID* m_idHelper {nullptr};
  SG::ReadHandleKey<McEventCollection> m_mcEventKey { this, "McEventCollection", "BeamTruthEvent" };
  SG::ReadHandleKey<FaserSiHitCollection> m_faserSiHitKey { this, "FaserSiHitCollection", "SCT_Hits" };
};


#endif // MCEVENTS_MCEVENTSALG_H
