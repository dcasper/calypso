/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "FaserMCTruth/FaserVTrackInformation.h"

FaserVTrackInformation::FaserVTrackInformation(TrackClassification tc):m_classify(tc),m_thePrimaryParticle(0)
{
}

const HepMC::GenParticle* FaserVTrackInformation::GetPrimaryHepMCParticle() const
{
  return m_thePrimaryParticle;
}

void FaserVTrackInformation::SetPrimaryHepMCParticle(const HepMC::GenParticle* p)
{
  m_thePrimaryParticle=p;
}


const HepMC::GenParticle* FaserVTrackInformation::GetHepMCParticle() const
{
  return 0;
}

const ISF::FaserISFParticle* FaserVTrackInformation::GetBaseISFParticle() const
{
  return 0;
}

bool FaserVTrackInformation::GetReturnedToISF() const
{
  return false;
}

void FaserVTrackInformation::SetParticle(const HepMC::GenParticle* /*p*/)
{
  // you should not call this, perhaps throw an exception?
  std::cerr<<"ERROR  FaserVTrackInformation::SetParticle() not supported  "<<std::endl;
 
}

void FaserVTrackInformation::SetBaseISFParticle(const ISF::FaserISFParticle* /*p*/)
{
  // you should not call this, perhaps throw an exception?
  std::cerr<<"ERROR  FaserVTrackInformation::SetBaseISFParticle() not supported  "<<std::endl;
 
}

void FaserVTrackInformation::SetReturnedToISF(bool)
{
  std::cerr<<"ERROR  FaserVTrackInformation::SetReturnedToISF() not supported  "<<std::endl;
}
