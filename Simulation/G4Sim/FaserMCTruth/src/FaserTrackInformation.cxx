/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "FaserMCTruth/FaserTrackInformation.h"
#include "AtlasHepMC/GenEvent.h"

FaserTrackInformation::FaserTrackInformation():m_regenerationNr(0),m_theParticle(0),m_theBaseISFParticle(0),m_returnedToISF(false)
{
}

FaserTrackInformation::FaserTrackInformation(const HepMC::GenParticle *p, const ISF::FaserISFParticle* baseIsp):
    m_regenerationNr(0),
    m_theParticle(p),
    m_theBaseISFParticle(baseIsp),
    m_returnedToISF(false)
{
}

const HepMC::GenParticle* FaserTrackInformation::GetHepMCParticle() const
{
  return m_theParticle;
}

const ISF::FaserISFParticle* FaserTrackInformation::GetBaseISFParticle() const
{
  return m_theBaseISFParticle;
}

int FaserTrackInformation::GetParticleBarcode() const
{
  return ( m_theParticle ? m_theParticle->barcode() : 0 );
}

void FaserTrackInformation::SetParticle(const HepMC::GenParticle* p)
{
  m_theParticle=p;
}

void FaserTrackInformation::SetBaseISFParticle(const ISF::FaserISFParticle* p)
{
  m_theBaseISFParticle=p;
}
